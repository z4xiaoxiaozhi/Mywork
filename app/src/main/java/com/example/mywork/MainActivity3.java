package com.example.mywork;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        Intent intent = getIntent();

        ArrayList<String> data = intent.getStringArrayListExtra("data");
        System.out.println(intent.getStringExtra("test"));
        TextView viewById = findViewById(R.id.names);
        viewById.setText(data.toString());
    }
    @Override
    protected void onStart() {
        Log.d("lifeline",this.getLocalClassName()+"start");
        super.onStart();
    }

    @Override
    protected void onStop() {
        Log.d("lifeline",this.getLocalClassName()+"停止");
        super.onStop();
    }
}