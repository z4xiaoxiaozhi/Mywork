package com.example.mywork;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {
    private MediaPlayer m;
    public MyService() {
    }

    @Override
    public void onCreate() {
        Log.d("life","service create");
        m=MediaPlayer.create(this,R.raw.music);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        if (m.isPlaying()){
            m.stop();
        }
        m.release();
        m=null;
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return new Musiccontroller();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public class Musiccontroller extends Binder{
        public void play(){
            m.start();
        }
        public void pause(){
            m.pause();
        }
        public long getDuration(){
            return  m.getDuration();
        }
        public long getposition(){
           return m.getCurrentPosition();
        }
        public void setPosition(int position){
            m.seekTo(position);
        }
    }
}