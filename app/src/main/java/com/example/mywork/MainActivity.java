package com.example.mywork;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Fragment w=new weixin();
    private Fragment s=new settings();
    private Fragment c=new contactpeople();
    private Fragment f=new friendcircle();

    private LinearLayout Lw;
    private LinearLayout Ls;
    private LinearLayout Lc;
    private LinearLayout Lf;

    private ImageView Bw;
    private ImageView Bs;
    private ImageView Bc;
    private ImageView Bf;

   // private ImageButton Bj;

    private FragmentManager fm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE );
        setContentView(R.layout.activity_main);

        initView();
        initFragment();
        initevent();
        selectFragment(0);

        //setdata();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId())
        {
            case R.id.id_tab_weixin:
            case R.id.imageButton1:
                selectFragment(0);
                break;
            case R.id.id_tab_contactpeople:
            case R.id.imageButton2:
                selectFragment(1);
               // jumpToac3();
                break;
            case R.id.id_tab_friendcircle:
            case R.id.imageButton3:
                selectFragment(2);
                break;
            case R.id.id_tab_settings:
            case R.id.imageButton4:
                selectFragment(3);
                break;
            default:
                break;
        }

    }
    private void initFragment(){
        fm=getSupportFragmentManager();
        FragmentTransaction transaction=fm.beginTransaction();
        transaction.add(R.id.id_content,w);
        transaction.add(R.id.id_content,s);
        transaction.add(R.id.id_content,c);
        transaction.add(R.id.id_content,f);
        transaction.commit();
    }
    private  void initView(){

        Lw=findViewById(R.id.id_tab_weixin);
        Ls=findViewById(R.id.id_tab_settings);
        Lc=findViewById(R.id.id_tab_contactpeople);
        Lf=findViewById(R.id.id_tab_friendcircle);

        Bw=findViewById(R.id.imageButton1);
        Bs=findViewById(R.id.imageButton2);
        Bc=findViewById(R.id.imageButton3);
        Bf=findViewById(R.id.imageButton4);

    }
    private void initevent(){
        Lw.setOnClickListener(this);
        Ls.setOnClickListener(this);
        Lc.setOnClickListener(this);
        Lf.setOnClickListener(this);



    }
    private void hideview(FragmentTransaction transaction){
        transaction.hide(w);
        transaction.hide(s);
        transaction.hide(c);
        transaction.hide(f);

    }
    private void selectFragment(int i){
        FragmentTransaction transaction=fm.beginTransaction();
        hideview(transaction);
        switch (i){
            case 0:
                transaction.show(w);

                break;
            case 1:
                transaction.show(c);
                //mImgFrd.setImageResource(R.drawable.tab_find_frd_pressed);
                break;
            case 2:
                transaction.show(f);
               // mImgContact.setImageResource(R.drawable.tab_address_pressed);
                break;
            case 3:
                transaction.show(s);
               // mImgSettings.setImageResource(R.drawable.tab_settings_pressed);
                break;
            default:
                break;
        }
        transaction.commit();
    }
//    public View.OnClickListener retlistener(){
//        return new myonclicklistener();
//    }





}