package com.example.mywork;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link contactpeople#newInstance} factory method to
 * create an instance of this fragment.
 */
public class contactpeople extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private RecyclerView recyclerView;
    private myadapter myadapter;
    private Context context;
    private ArrayList<HashMap<String,Object>> data=new ArrayList<HashMap<String,Object>>();


    public contactpeople() {
        // Required empty public constructor
    }
    public static contactpeople newInstance(String param1, String param2) {
        contactpeople fragment = new contactpeople();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_contactpeople, container, false);
        context=inflate.getContext();
        recyclerView=inflate.findViewById(R.id.recycleview);
//
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("name","张小致");
        stringObjectHashMap.put("number",138999);
        stringObjectHashMap.put("type","男");
        HashMap<String, Object> stringObjectHashMap1 = new HashMap<>();
        stringObjectHashMap1.put("name","朱小兵");
        stringObjectHashMap1.put("number",138999);
        stringObjectHashMap1.put("type","男");
        HashMap<String, Object> stringObjectHashMap2 = new HashMap<>();
        stringObjectHashMap2.put("name","左小萌");
        stringObjectHashMap2.put("number",138999);
        stringObjectHashMap2.put("type","女");



        //System.out.println(data.size());
        data.add(stringObjectHashMap);
        data.add(stringObjectHashMap1);
        data.add(stringObjectHashMap2);
        myadapter=new myadapter(context,data);
        //LinearLayoutManager manager=new LinearLayoutManager(context);
        StaggeredGridLayoutManager manager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(myadapter);

        return inflate;
    }


}