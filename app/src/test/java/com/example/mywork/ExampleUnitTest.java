package com.example.mywork;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void dbtest(){
        ArrayList<HashMap<String, Object>> infoByName = DBUtil.getInfoByName("select * from ascontact");
        for (HashMap<String,Object> value:infoByName
             ) {
            System.out.println(value.get("name"));
        }
    }
}